<?php

namespace Drupal\commerce_peach\PluginForm;

use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;

class PeachCheckoutForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $payment = $this->entity;
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $conf = $payment_gateway_plugin->getConfiguration();

    $peach_url = $conf['peach_test_url'];
    $entity_id = $conf['test_entity_id'];
    $access_token = $conf['test_access_token'];
    if ($conf['transaction_mode'] == 'live') {
      $peach_url = $conf['peach_live_url'];
      $entity_id = $conf['live_entity_id'];
      $access_token = $conf['live_access_token'];
    }
    
    $order = $payment->getOrder();
    $order->save();

    $order_total = number_format((float)$payment->getAmount()->getNumber()*100., 0, '.', '');

    $data = [];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, sprintf("%s/v1/checkouts", $peach_url));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(sprintf('Authorization:Bearer %s', $access_token)));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, sprintf("entityId=%s&amount=%s&currency=%s&paymentType=DB", $entity_id, $order_total, $conf['transaction_currency']));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $conf['transaction_mode'] == 'live' ? true : false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $responseData = json_decode(curl_exec($ch));

    if(curl_errno($ch)) {
      return curl_error($ch);
    }

    curl_close($ch);

    $checkoutId = $responseData->id;

    $form['#attached']['library'][] = sprintf('%s/v1/paymentWidgets.js?checkoutId=%s', $peach_url, $checkoutId);
    $form['#action'] = $form['#return_url'];
    $form['#attributes']['data-brands'] = $conf['transaction_brands'];
    $form['#attributes']['class'] = ['paymentWidgets'];

    return $this->buildRedirectForm($form, $form_state, $form['#return_url'], $data, 'post');
  }
}
