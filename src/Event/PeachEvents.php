<?php

namespace Drupal\commerce_peach\Event;

/**
 * Defines events for the Commerce Peach module.
 */
final class PeachEvents {

  /**
   * Name of the event fired when performing the Peach Checkout requests.
   *
   * @Event
   *
   * @see \Drupal\commerce\Event\PeachCheckoutRequestEvent.php
   */
  const PEACH_CHECKOUT_REQUEST = 'commerce_peach.peach_checkout';

}
